using System;
using System.Collections.Generic;
using System.Linq;

namespace TenThousandNumbers.Domain {
    public interface IListRandomizer {
        IEnumerable<int> Randomize(IEnumerable<int> numberList);
    }

    public class ListRandomizer : IListRandomizer {
        readonly Random _random;

        public ListRandomizer(Random random) {
            _random = random;
        }

        public virtual IEnumerable<int> Randomize(IEnumerable<int> numberList) {
            return numberList.OrderBy(item => _random.Next(int.MinValue, int.MaxValue)).ToList();
        }
    }
}