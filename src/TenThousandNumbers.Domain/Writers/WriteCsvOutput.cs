﻿using System.Collections;
using System.IO;
using TenThousandNumbers.Domain.Config;

namespace TenThousandNumbers.Domain.Writers {
    public class WriteCsvOutput : IWriteOutput {
        readonly IConfiguration _configuration;

        public WriteCsvOutput(IConfiguration configuration) {
            _configuration = configuration;
        }

        public void Write(IEnumerable lines) {
            var csvFilePath = _configuration.GetCsvFilePath();
            var csvFile = new FileInfo(csvFilePath);
            var fileStream = csvFile.Exists ? csvFile.OpenWrite() : csvFile.Create();
                
            using (var writer = new StreamWriter(fileStream)) {
                foreach (var line in lines) {
                    writer.WriteLine(line);
                }
            }
        }
    }
}