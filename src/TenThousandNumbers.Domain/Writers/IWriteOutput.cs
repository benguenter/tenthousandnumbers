﻿using System.Collections;

namespace TenThousandNumbers.Domain.Writers {
    public interface IWriteOutput {
        void Write(IEnumerable lines);
    }
}