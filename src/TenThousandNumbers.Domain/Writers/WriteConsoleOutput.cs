﻿using System.Collections;

namespace TenThousandNumbers.Domain.Writers {
    public class WriteConsoleOutput : IWriteOutput {
        public void Write(IEnumerable lines) {
            foreach (var line in lines) {
                System.Console.Out.WriteLine(line);
            }
        }
    }
}