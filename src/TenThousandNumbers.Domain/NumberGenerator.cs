using System.Collections.Generic;
using System.Linq;
using TenThousandNumbers.Domain.Config;

namespace TenThousandNumbers.Domain {
    public interface INumberGenerator {
        IEnumerable<int> GetNumbers();
    }

    public class NumberGenerator : INumberGenerator {
        readonly IConfiguration _configuration;

        public NumberGenerator(IConfiguration configuration) {
            _configuration = configuration;
        }

        public virtual IEnumerable<int> GetNumbers()
        {
            //The second part of the Enumerable.Range is the number of sequential integers to add
            //because of this, we must add +1 at the end so that the generation is inclusive of the max number
            var countOfNumbersToAdd = _configuration.GetMaxNumber() - _configuration.GetMinNumber() + 1;
            return Enumerable.Range(_configuration.GetMinNumber(),countOfNumbersToAdd);
        }
    }
}