﻿using System.Collections.Generic;
using TenThousandNumbers.Domain.Writers;

namespace TenThousandNumbers.Domain {
    public interface IGenerateUniqueNumbersInRandomOrder {
        void Generate();
    }

    public class GenerateUniqueNumbersInRandomOrder : IGenerateUniqueNumbersInRandomOrder {
        readonly INumberGenerator _numberGenerator;
        readonly IListRandomizer _listRandomizer;
        readonly IEnumerable<IWriteOutput> _writers;

        public GenerateUniqueNumbersInRandomOrder(INumberGenerator numberGenerator, IListRandomizer listRandomizer, IEnumerable<IWriteOutput> writers) {
            _numberGenerator = numberGenerator;
            _listRandomizer = listRandomizer;
            _writers = writers;
        }

        public void Generate() {
            var numbers = _numberGenerator.GetNumbers();
            var randomizedListOfNumbers = _listRandomizer.Randomize(numbers);
            foreach (var writer in _writers) {
                writer.Write(randomizedListOfNumbers);
            }
        }
    }
}