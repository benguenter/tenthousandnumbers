﻿using System;
using System.Configuration;

namespace TenThousandNumbers.Domain.Config {
    public class Configuration : IConfiguration {
        public int GetMinNumber() {
            return Convert.ToInt32(GetConfigValue("minNumberToGenerate"));
        }

        public int GetMaxNumber() {
            return Convert.ToInt32(GetConfigValue("maxNumberToGenerate"));
        }

        public string GetCsvFilePath() {
            return GetConfigValue("csvFilePath");
        }

        string GetConfigValue(string key) {
            return ConfigurationManager.AppSettings[key];
        }
    }
}