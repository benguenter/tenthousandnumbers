﻿namespace TenThousandNumbers.Domain.Config {
    public interface IConfiguration {
        int GetMinNumber();
        int GetMaxNumber();
        string GetCsvFilePath();
    }
}