﻿using System;
using Autofac;
using TenThousandNumbers.Domain;

namespace TenThousandNumbers.Console.IOC {
    public class InitializeIoc {
        public static IContainer Start() {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyTypes(typeof (DomainAssemblyMarker).Assembly).AsImplementedInterfaces();
            builder.RegisterType<Random>();

            return builder.Build();
        } 
    }
}