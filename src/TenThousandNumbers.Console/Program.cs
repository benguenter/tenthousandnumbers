﻿using System;
using Autofac;
using TenThousandNumbers.Console.IOC;
using TenThousandNumbers.Domain;

namespace TenThousandNumbers.Console
{
    class Program
    {
        static void Main(string[] args) {
            var container = InitializeIoc.Start();
            var generateUniqueNumbers = container.Resolve<IGenerateUniqueNumbersInRandomOrder>();

            try {
                //By default this program will output a list of 10,000 numbers from 1 to 10,000 inclusive to the console as well as a csv file located in the bin folder
                //To change these parameters, change the settings located in the app.config file in the TenThousandNumbers.Console project
                generateUniqueNumbers.Generate();
            } catch (Exception e){
                System.Console.Out.Write(e);
            } 

            System.Console.Out.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }
    }
}
