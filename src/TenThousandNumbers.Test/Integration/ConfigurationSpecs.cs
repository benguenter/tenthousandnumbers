﻿using System;
using System.Configuration;
using Machine.Specifications;
using Configuration = TenThousandNumbers.Domain.Config.Configuration;

namespace TenThousandNumbers.Test.Integration {
    public class ConfigurationSpecs {
        public class when_getting_configs {
            //Because these are integration tests we will grab the data right from the app config so that if it ever changes in the future these won't break
            It should_return_the_value_specified_in_the_app_config_for_the_min_number = 
                () => sut.GetMinNumber().ShouldEqual(Convert.ToInt32(ConfigurationManager.AppSettings["minNumberToGenerate"]));

            It should_return_the_value_specified_in_the_app_config_for_the_max_number = 
                () => sut.GetMaxNumber().ShouldEqual(Convert.ToInt32(ConfigurationManager.AppSettings["maxNumberToGenerate"]));

            It should_return_the_value_specified_in_the_app_config_for_the_csv_file_ath = 
                () => sut.GetCsvFilePath().ShouldEqual(ConfigurationManager.AppSettings["csvFilePath"]);

            Establish context = () => sut = new Configuration();

            static Configuration sut;
        }
    }
}