﻿using System.IO;
using Machine.Specifications;
using TenThousandNumbers.Domain.Config;
using TenThousandNumbers.Domain.Writers;
using It = Machine.Specifications.It;

namespace TenThousandNumbers.Test.Integration {
    public class WriteCsvOutputSpecs {
        public class when_writing_to_a_csv_file {
            //because we are interacting with the file system here, this is an integration test
            It should_write_the_line = () => {
                var fileInfo = new FileInfo(_configuration.GetCsvFilePath()).OpenRead();
                var streamReader = new StreamReader(fileInfo);
                streamReader.ReadLine().ShouldEqual(_output);
                streamReader.Close();
            };

            Establish context = () => _sut = new WriteCsvOutput(_configuration);

            Because of = () => _sut.Write(new[] {_output});

            static WriteCsvOutput _sut;
            static string _output = "Test";
            static Configuration _configuration = new Configuration();
        }
    }
}