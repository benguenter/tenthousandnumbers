﻿using System.Collections.Generic;
using System.Linq;
using Machine.Specifications;
using Moq;
using TenThousandNumbers.Domain;
using TenThousandNumbers.Domain.Config;
using It = Machine.Specifications.It;

namespace TenThousandNumbers.Test.Unit {
    public class NumberGeneratorSpecs {
        public class when_generating_numbers {
            It should_generate_numbers_according_to_the_configuration = () => _results.Count().ShouldEqual(_maxNumber);

            It should_include_the_min_number = () => _results.ShouldContain(_minNumber);

            It should_include_the_max_number = () => _results.ShouldContain(_maxNumber);

            Establish context = () => {
                _configuration = new Mock<IConfiguration>();
                _configuration.Setup(config => config.GetMinNumber()).Returns(_minNumber);
                _configuration.Setup(config => config.GetMaxNumber()).Returns(_maxNumber);

                _sut = new NumberGenerator(_configuration.Object);
            };

            Because of = () => _results = _sut.GetNumbers();

            static NumberGenerator _sut;
            static Mock<IConfiguration> _configuration;
            static IEnumerable<int> _results;
            static int _minNumber = 1;
            static int _maxNumber = 2;
        }
    }
}