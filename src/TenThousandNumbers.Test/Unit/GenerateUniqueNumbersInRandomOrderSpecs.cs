﻿using System.Collections.Generic;
using Machine.Specifications;
using Moq;
using TenThousandNumbers.Domain;
using TenThousandNumbers.Domain.Writers;
using It = Machine.Specifications.It;

namespace TenThousandNumbers.Test.Unit
{
    public class GenerateUniqueNumbersInRandomOrderSpecs
    {
        public class when_generating_ten_thousand_numbers
        {
            It should_request_a_list_of_numbers = () => _numberGenerator.Verify(generator => generator.GetNumbers());

            It should_randomize_the_list = () => _listRandomizer.Verify(randomizer => randomizer.Randomize(_numberList));

            It should_ask_the_writers_to_output_the_list = () => _outputWriter.Verify(writer => writer.Write(_randomizedNumberList));

            Establish context = () =>
            {
                _numberGenerator = new Mock<INumberGenerator>();
                _numberGenerator.Setup(generator => generator.GetNumbers()).Returns(_numberList);

                _listRandomizer = new Mock<IListRandomizer>();
                _listRandomizer.Setup(randomizer => randomizer.Randomize(_numberList)).Returns(_randomizedNumberList);

                _outputWriter = new Mock<IWriteOutput>();

                _sut = new GenerateUniqueNumbersInRandomOrder(_numberGenerator.Object, _listRandomizer.Object, new [] {_outputWriter.Object});
            };

            Because of = () => _sut.Generate();

            static Mock<INumberGenerator> _numberGenerator;
            static Mock<IListRandomizer> _listRandomizer;
            static IEnumerable<int> _numberList = new List<int>();
            static IEnumerable<int> _randomizedNumberList = new List<int>();
            static GenerateUniqueNumbersInRandomOrder _sut;
            static Mock<IWriteOutput> _outputWriter;
        }
    }
}