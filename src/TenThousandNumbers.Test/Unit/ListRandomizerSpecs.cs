﻿using System;
using System.Collections.Generic;
using System.Linq;
using Machine.Specifications;
using Moq;
using TenThousandNumbers.Domain;
using It = Machine.Specifications.It;

namespace TenThousandNumbers.Test.Unit {
    public class ListRandomizerSpecs {
        It should_randomize_according_to_the_values_returned_by_the_random_object = () => {
            _results.First().ShouldEqual(secondNumber);
            _results.Last().ShouldEqual(firstNumber);
        };

        Establish context = () => {
            _testList = new List<int> {firstNumber,secondNumber};

            _random = new Mock<Random>();
            //this will return 500 the first time it's called and 250 the second to simulate ordering
            _random.SetupSequence(random => random.Next(int.MinValue, int.MaxValue)).Returns(500).Returns(250);

            _sut = new ListRandomizer(_random.Object);
        };

        Because of = () => _results = _sut.Randomize(_testList);

        static ListRandomizer _sut;
        static IEnumerable<int> _testList;
        static IEnumerable<int> _results;
        static Mock<Random> _random;
        static int firstNumber = 1;
        static int secondNumber = 2;
    }
}