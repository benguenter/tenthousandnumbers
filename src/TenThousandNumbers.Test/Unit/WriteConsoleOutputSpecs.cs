﻿using System.IO;
using Machine.Specifications;
using TenThousandNumbers.Domain.Writers;
using It = Machine.Specifications.It;

namespace TenThousandNumbers.Test.Unit {
    public class WriteConsoleOutputSpecs {
        public class when_writing_output_to_the_console {
            It should_write_the_output = () => {
                _streamWriter.Flush();
                _outputStream.Position = 0L;
                var reader = new StreamReader(_outputStream);
                reader.ReadLine().ShouldEqual(_output);
            };

            Establish context = () => {
                _outputStream = new MemoryStream();
                _streamWriter = new StreamWriter(_outputStream);
                System.Console.SetOut(_streamWriter);

                _sut = new WriteConsoleOutput();
            };

            Because of = () => _sut.Write(new [] { _output });

            static WriteConsoleOutput _sut;
            static string _output = "Test";
            static MemoryStream _outputStream;
            static StreamWriter _streamWriter;
        }
    }
}